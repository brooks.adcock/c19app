#!/usr/bin/env bash

docker stop c19app
docker rm c19app
docker build -f Dockerfile-prod -t c19app:prod .