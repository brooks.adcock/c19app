#!/usr/bin/env bash

aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 719089624082.dkr.ecr.us-east-1.amazonaws.com


docker stop c19app
docker rm c19app
docker image rm c19app:prod
docker build -f Dockerfile-prod -t c19app:prod .

docker tag c19app:prod 719089624082.dkr.ecr.us-east-1.amazonaws.com/c19app:latest

docker push 719089624082.dkr.ecr.us-east-1.amazonaws.com/c19app:latest

aws ecs update-service --force-new-deployment --cluster c19v3-cluster --service c19v3-service --task-definition c19v3-taskdefinition:1

