#!/usr/bin/env bash

docker stop c19app
docker rm c19app
docker build -f Dockerfile-dev -t c19app:dev .