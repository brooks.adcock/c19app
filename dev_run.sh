#!/usr/bin/env bash


docker stop c19app
docker rm c19app
docker run -p 8080:8080 -v /Users/brooksadcock/Documents/Projects/C19App/App:/app --name c19app c19app:dev
